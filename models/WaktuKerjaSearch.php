<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WaktuKerja;

/**
 * WaktuKerjaSearch represents the model behind the search form about `app\models\WaktuKerja`.
 */
class WaktuKerjaSearch extends WaktuKerja
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_karyawan', 'is_deleted'], 'integer'],
            [['date', 'jam_awal', 'jam_pulang', 'efektifitas'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WaktuKerja::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_karyawan' => $this->id_karyawan,
            'date' => $this->date,
            'jam_awal' => $this->jam_awal,
            'jam_pulang' => $this->jam_pulang,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['like', 'efektifitas', $this->efektifitas]);

        return $dataProvider;
    }
}
