<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "waktu_kerja".
 *
 * @property integer $id
 * @property integer $id_karyawan
 * @property string $date
 * @property string $jam_awal
 * @property string $jam_pulang
 * @property string $efektifitas
 * @property integer $is_deleted
 *
 * @property Karyawan $idKaryawan
 */
class WaktuKerja extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'waktu_kerja';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_karyawan', 'date', 'jam_awal', 'jam_pulang', 'efektifitas'], 'required'],
            [['id_karyawan', 'is_deleted'], 'integer'],
            [['date', 'jam_awal', 'jam_pulang'], 'safe'],
            [['efektifitas'], 'string'],
            [['id_karyawan'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['id_karyawan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_karyawan' => 'Id Karyawan',
            'date' => 'Date',
            'jam_awal' => 'Jam Awal',
            'jam_pulang' => 'Jam Pulang',
            'efektifitas' => 'Efektifitas',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdKaryawan()
    {
        return $this->hasOne(Karyawan::className(), ['id' => 'id_karyawan']);
    }
}
