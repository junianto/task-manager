<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pekerjaan".
 *
 * @property integer $id
 * @property string $nama
 * @property string $deskripsi
 * @property string $tanggal
 * @property string $waktu_mulai
 * @property string $waktu_selesai
 * @property integer $id_karyawan
 * @property integer $is_deleted
 *
 * @property Karyawan $idKaryawan
 */
class Pekerjaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pekerjaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'deskripsi', 'tanggal', 'waktu_mulai', 'waktu_selesai', 'id_karyawan'], 'required'],
            [['deskripsi'], 'string'],
            [['tanggal', 'waktu_mulai', 'waktu_selesai'], 'safe'],
            [['id_karyawan', 'is_deleted'], 'integer'],
            [['nama'], 'string', 'max' => 30],
            [['id_karyawan'], 'exist', 'skipOnError' => true, 'targetClass' => Karyawan::className(), 'targetAttribute' => ['id_karyawan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'deskripsi' => 'Deskripsi',
            'tanggal' => 'Tanggal',
            'waktu_mulai' => 'Waktu Mulai',
            'waktu_selesai' => 'Waktu Selesai',
            'id_karyawan' => 'Id Karyawan',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdKaryawan()
    {
        return $this->hasOne(Karyawan::className(), ['id' => 'id_karyawan']);
    }
}
